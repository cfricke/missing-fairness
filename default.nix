with import <nixpkgs> { };

let
  pythonPackages = python3Packages;
in pkgs.mkShell rec {
  name = "impureThesisPythonEnv";
  venvDir = "./.venv";
  buildInputs = [
    pythonPackages.python
    pythonPackages.venvShellHook

    pythonPackages.black
    pythonPackages.flake8
    pythonPackages.ipdb
    pythonPackages.numpy
    pythonPackages.pandas
    pythonPackages.pyarrow
    pythonPackages.scikitlearn
    pythonPackages.scipy
    pythonPackages.seaborn
  ];

  # Now we can execute any commands within the virtual environment.
  # This is optional and can be left out to run pip manually.
  # postShellHook = ''
  #   pip install -r requirements.txt
  # '';
}
