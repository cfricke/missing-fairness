# Imports
# :PROPERTIES:
# :header-args: :results silent
# :END:

# Default data and plotting libraries


# [[file:classification.org::*Imports][Imports:1]]
import os
from typing import List

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from sklearn.experimental import enable_iterative_imputer # noqa
from sklearn.cluster import AgglomerativeClustering, KMeans
from sklearn.decomposition import PCA
from sklearn.ensemble import RandomForestClassifier
from sklearn.impute import IterativeImputer, SimpleImputer
from sklearn.inspection import permutation_importance
from sklearn.linear_model import BayesianRidge
from sklearn.metrics import make_scorer, matthews_corrcoef
from sklearn.model_selection import RandomizedSearchCV, StratifiedShuffleSplit
from sklearn.preprocessing import OneHotEncoder, OrdinalEncoder
from sklearn.tree import DecisionTreeClassifier, export_graphviz


sns.set(style="whitegrid", palette="muted", color_codes=True)
palette = sns.color_palette()
# Imports:1 ends here



# Package files


# [[file:classification.org::*Imports][Imports:2]]
from metrics import d_all
from utils import one_hot_encode, score_and_print, print_tab
# Imports:2 ends here

# Load

# Merged users and applications data sets containing applications that have been
# either accepted or rejected.


# [[file:classification.org::*Load][Load:1]]
df = pd.read_parquet("../data/law-school-numbers/merged.parquet")
df.fillna(value=np.nan, inplace=True)

# categorical attributes requiring encoding
attrs_enc = [
    "cycle",
    "school",
    "state",
    "years_out",
]

# attributes to fit a model with
attrs_incl = [
    "cycle",
    "early_decision",
    "fee_waiver",
    "gpa",
    "international",
    "lsat",
    "non_traditional",
    # "scholarship",
    "school",
    "state",
    "teach_for_america",
    "veteran",
    "years_out",
]

attrs_sens = [
    "african_american",
    "lgbt",
    "sex",
    "minority",
]

sv_map = {
    "african_american": [0, 1],
    "lgbt": [0, 1],
    "minority": [0, 1],
    "sex": ["male", "female"],
}

label = "accepted"
# Load:1 ends here



# Get and print missing attributes.


# [[file:classification.org::*Load][Load:2]]
attrs_missing = df.columns[df.isnull().any()]
print(attrs_missing)
# Load:2 ends here

# Encode categorical data

# Set =encoding= to either =ordinal= for numeric encoding, or =one-hot= for one-hot
# encoding of data.


# [[file:classification.org::*Encode categorical data][Encode categorical data:1]]
encoding = "ordinal"
de = df.copy()

if encoding == "ordinal":
    encoder = OrdinalEncoder(dtype=np.float64)
    for a in attrs_enc:
        encoded = encoder.fit_transform(
            np.array(de[a].dropna()).reshape(-1, 1)
        )
        de.loc[de[a].notnull(), a] = np.squeeze(encoded)
        de[a] = de[a].astype(np.float64)

print(de.info())
# Encode categorical data:1 ends here

# Scale features

# Does make much of a difference for our dataset, as we are using classification
# and not regression trees and our data is mostly categorical / binary, with the
# exception of =gpa= and =lsat=.


# [[file:classification.org::*Scale features][Scale features:1]]
# from sklearn.preprocessing import StandardScaler

# scaler = StandardScaler()
# de[attrs_incl] = scaler.fit_transform(de[attrs_incl])
# Scale features:1 ends here

# Sample complete-case


# [[file:classification.org::*Sample complete-case][Sample complete-case:1]]
dc = de.dropna(subset=attrs_incl)
attrs_incl_cc = attrs_incl.copy()
if encoding == "one-hot":
    dc, attrs_incl_cc = one_hot_encode(dc, attrs_enc, attrs_incl)
# Sample complete-case:1 ends here

# Cluster

# Sample down further to feasibly cluster data.  We remove the most discerning
# continuous attributes (=gpa= and =lsat=) to create a pure binary data set.
# Additionally, we include the (one-hot encoded) sensitive attributes for
# clustering only.


# [[file:classification.org::*Cluster][Cluster:1]]
dc_cl = dc.sample(10000).copy()
attrs_incl_cl = attrs_incl_cc.copy()
attrs_incl_cl.remove("gpa")
attrs_incl_cl.remove("lsat")
# Cluster:1 ends here

# Agglomerative Clustering

# Try a hierarchical clustering method.


# [[file:classification.org::*Agglomerative Clustering][Agglomerative Clustering:1]]
clustering = AgglomerativeClustering(
    n_clusters=4, affinity="manhattan", linkage="complete"
).fit(dc_cl[attrs_incl_cl])

dc_cl["cluster"] = clustering.labels_

# Visualize clusters
fig = plt.figure(figsize=(5, 5))
with sns.axes_style("ticks"):
    axes = fig.add_subplot()
    g = sns.pairplot(
        dc_cl[["gpa", "lsat", "cluster"]], hue="cluster", diag_kind="hist"
    )

# Set text labels
g.fig.suptitle(f"Agglomerative clusters, n={len(dc_cl)}", y=1.04)

fname = "agglomerative_clusters.png"
fpath = f"../doc/figures/{fname}"
g.savefig(fpath, dpi=150)
plt.close(g.fig)
fpath
# Agglomerative Clustering:1 ends here

# K-Means Elbow Curve


# [[file:classification.org::*K-Means Elbow Curve][K-Means Elbow Curve:1]]
nc = range(1, 20)
kmeans = [KMeans(n_clusters=i) for i in nc]
sample = dc_cl[attrs_incl_cl].sample(frac=0.5)
scores = [kmeans[i].fit(sample).score(sample) for i in range(len(kmeans))]

# Plot
with sns.axes_style("whitegrid"):
    fig, axes = plt.subplots(figsize=(5, 5))
    sns.lineplot(x="x", y="y", data=pd.DataFrame({"x": nc, "y": scores}))

# Set text labels
plt.xlabel("Number of Clusters")
plt.ylabel("Score")
plt.title("Elbow Curve", fontsize=14, pad=10)
plt.tight_layout()

fname = "kmeans_elbow_curve.png"
fpath = f"../doc/figures/{fname}"
fig.savefig(fpath, dpi=150)
plt.close()
fpath
# K-Means Elbow Curve:1 ends here

# K-Means PCA


# [[file:classification.org::*K-Means PCA][K-Means PCA:1]]
pca = PCA(n_components=10).fit(dc_cl[attrs_incl_cl])
d_p = pca.transform(dc_cl[attrs_incl_cl])
km = KMeans(n_clusters=4, init="k-means++")
km.fit(d_p)
dc_cl["cluster"] = km.predict(d_p)
# K-Means PCA:1 ends here

# K-Means Visualization


# [[file:classification.org::*K-Means Visualization][K-Means Visualization:1]]
fig = plt.figure(figsize=(5, 5))
with sns.axes_style("ticks"):
    axes = fig.add_subplot()
    g = sns.pairplot(
        dc_cl[["gpa", "lsat", "cluster"]], hue="cluster", diag_kind="hist"
    )

# Set text labels
g.fig.suptitle(f"K-Means clusters, n={len(d_p)}", y=1.04)

fname = "kmeans_clusters.png"
fpath = f"../doc/figures/{fname}"
g.savefig(fpath, dpi=150)
plt.close(g.fig)
fpath
# K-Means Visualization:1 ends here

# Decision Tree Plot


# [[file:classification.org::*Decision Tree Plot][Decision Tree Plot:1]]
m = DecisionTreeClassifier(
    criterion="entropy",
    max_depth=25,
    min_samples_split=8,
    min_samples_leaf=7,
    random_state=0,
    class_weight="balanced",
)
sss = StratifiedShuffleSplit(
    n_splits=1, test_size=1 / np.sqrt(len(attrs_incl_cc)), random_state=0
)
tr_idx, te_idx = next(sss.split(dc[attrs_incl_cc], dc[label]))
dtr = dc.iloc[tr_idx, :]
dte = dc.iloc[te_idx, :]
ytr = dtr[label].values
xtr = dtr[attrs_incl_cc].values
yte = dte[label].values
xte = dte[attrs_incl_cc].values

m.fit(xtr, ytr)

ftree = f"decision_tree_{encoding}.dot"
export_graphviz(
    m,
    out_file=ftree,
    max_depth=2,
    feature_names=attrs_incl_cc,
)

fname = f"decision_tree_{encoding}.png"
fpath = f"../doc/figures/{fname}"
os.system(f"dot -x -Tpng {ftree} -o {fpath}")
os.remove(ftree)
fpath
# Decision Tree Plot:1 ends here

# Random Forest Feature Importance


# [[file:classification.org::*Random Forest Feature Importance][Random Forest Feature Importance:1]]
m = RandomForestClassifier(
    n_estimators=50,
    criterion="entropy",
    max_depth=50,
    min_samples_split=10,
    min_samples_leaf=2,
    max_features=0.5,
    n_jobs=-1,
    random_state=0,
    class_weight="balanced",
    # ccp_alpha=1e-15,
    max_samples=0.25,
)
sss = StratifiedShuffleSplit(
    n_splits=1, test_size=1 / np.sqrt(len(attrs_incl_cc)), random_state=0
)
attrs_incl_vi = attrs_incl.copy()
tr_idx, te_idx = next(sss.split(de[attrs_incl_vi], de[label]))
dtr = de.iloc[tr_idx, :]
dte = de.iloc[te_idx, :]
ytr = dtr[label].values
xtr = dtr[attrs_incl_vi].values
yte = dte[label].values
xte = dte[attrs_incl_vi].values

imputer = SimpleImputer(strategy="most_frequent")
imputer.fit(xtr)
xtr = imputer.transform(xtr)
xte = imputer.transform(xte)

m.fit(xtr, ytr)
sorted_idx = m.feature_importances_.argsort()

perm_imp = permutation_importance(
    m,
    xte,
    yte,
    scoring=make_scorer(matthews_corrcoef),
    n_repeats=10,
    random_state=0
)
perm_sorted_idx = perm_imp.importances_mean.argsort()

# Plot
fig = plt.figure(figsize=(9, 5))
with sns.axes_style("ticks"):
    axes = fig.add_subplot(1, 2, 1)
    g = sns.barplot(
        x=m.feature_importances_[sorted_idx][-20:],
        y=np.array(attrs_incl_vi)[sorted_idx][-20:],
        palette=sns.color_palette("Set1", 2)[1:],
        ax=axes,
    )
    [
        axes.text(
            p.get_x() + p.get_width() + 0.01,
            p.get_y() + p.get_height() - 0.2,
            "{:.4f}".format(p.get_width()),
            ha="left"
        )
        for p in g.patches
    ]
    sns.despine(top=True, right=True)
    axes.tick_params(length=4, labelsize=12)
    plt.xlim(0, 0.4)
    plt.title("Entropy", fontsize=14, pad=10)
    axes = fig.add_subplot(1, 2, 2)
    sns.boxplot(
        data=pd.DataFrame(
            perm_imp.importances[perm_sorted_idx][-20:],
            index=np.array(attrs_incl_vi)[perm_sorted_idx][-20:]
        ).T,
        # palette=sns.color_palette("Set1", 2)[:1],
        orient='h',
        ax=axes,
    )
    sns.despine(top=True, right=True)
    axes.tick_params(length=4, labelsize=12)
    plt.title("Permutation", fontsize=14, pad=10)
    plt.tight_layout()
    fig.text(0.5, 0.025, "Variable Importance", fontsize=14, ha="center", weight="bold")
    plt.gcf().subplots_adjust(bottom=0.18)

fname = f"random_forest_feature_importances.png"
fpath = f"../doc/figures/{fname}"
fig.savefig(fpath, dpi=300)
plt.close()
fpath
# Random Forest Feature Importance:1 ends here

# Hyper-parameter Tuning


# [[file:classification.org::*Hyper-parameter Tuning][Hyper-parameter Tuning:1]]
parameters = {
    "n_estimators": [10, 50, 100],
    "max_depth": [None, 10, 25, 50, 100],
    "min_samples_split": range(2, 11),
    "min_samples_leaf": range(1, 11),
    "criterion": ["entropy", "gini"],
    "max_features": ["log2", "sqrt", 0.5],
    "random_state": [0],
    "class_weight": [None, "balanced"],
}
sss = StratifiedShuffleSplit(
    n_splits=10, test_size=1 / np.sqrt(len(attrs_incl_cc)), random_state=0
)
clf = RandomizedSearchCV(
    RandomForestClassifier(),
    # DecisionTreeClassifier(),
    parameters,
    n_iter=100,
    scoring=make_scorer(matthews_corrcoef),
    n_jobs=-1,
    cv=sss,
    random_state=0,
)
clf.fit(X=dc[attrs_incl_cc], y=dc.accepted)
tree_model = clf.best_estimator_

print("|-|")
print_tab("score", f"{clf.best_score_:.4f}")
print("|-|")
for k, v in clf.best_params_.items():
    print_tab(k, str(v))

print("|-|")
# Hyper-parameter Tuning:1 ends here

# Complete-Case Analysis with Random Forests

# Separate into train, test for input =x= and outcome =y=.  We split the data set into 10
# folds of =[(train, test)]= data.


# [[file:classification.org::*Complete-Case Analysis with Random Forests][Complete-Case Analysis with Random Forests:1]]
from importlib import reload
import utils

reload(utils)
acc_cc, d_all_cc = utils.score_and_print(
# acc_cc, d_all_cc = score_and_print(
    df=dc,
    label=label,
    imputation=None,
    attrs_incl=attrs_incl_cc,
    attrs_sens=attrs_sens,
    sv_map=sv_map,
)
# Complete-Case Analysis with Random Forests:1 ends here

# Simple Imputation


# [[file:classification.org::*Simple Imputation][Simple Imputation:1]]
imputation = None if encoding == "one-hot" else "simple"
# Simple Imputation:1 ends here

# All Data


# [[file:classification.org::*All Data][All Data:1]]
dis = de.copy()
attrs_incl_is = attrs_incl.copy()

# One-hot encoding requires imputation prior to splitting
if encoding == "one-hot":
    imputer = SimpleImputer(strategy="most_frequent")
    dis = pd.DataFrame(
        imputer.fit_transform(de[attrs_incl]), columns=de[attrs_incl].columns
    )
    dis, attrs_incl_is = one_hot_encode(dis, attrs_enc, attrs_incl)

    # Include remaining
    for a in attrs_sens:
        dis[a] = de[a]

    dis[label] = de[label]

acc_is, d_all_is = score_and_print(
    df=dis,
    label=label,
    imputation=imputation,
    attrs_incl=attrs_incl_is,
    # attrs_incl=list(set(attrs_incl_is) - {"years_out", "state"}),
    attrs_sens=attrs_sens,
    sv_map=sv_map,
)
# All Data:1 ends here

# Only Missing Data

# Exclude complete cases for simple imputation.


# [[file:classification.org::*Only Missing Data][Only Missing Data:1]]
dism = dis.loc[dis.index.difference(dc.index), :]
attrs_incl_ism = attrs_incl_is.copy()

# Print table
acc_ism, d_all_ism = score_and_print(
    df=dism,
    label=label,
    imputation=imputation,
    attrs_incl=attrs_incl_ism,
    attrs_sens=attrs_sens,
    sv_map=sv_map,
)
# Only Missing Data:1 ends here

# Iterative Imputation


# [[file:classification.org::*Iterative Imputation][Iterative Imputation:1]]
imputation = None if encoding == "one-hot" else "iterative"
# Iterative Imputation:1 ends here

# All Data


# [[file:classification.org::*All Data][All Data:1]]
dii = de.copy()
attrs_incl_ii = attrs_incl.copy()

# One-hot encoding requires imputation prior to splitting, as well as
# numerically encoded categories for iterative imputation
if encoding == "one-hot":
    encoder = OrdinalEncoder(dtype=np.float64)
    for a in attrs_enc:
        encoded = encoder.fit_transform(
            np.array(dii[a].dropna()).reshape(-1, 1)
        )
        dii.loc[dii[a].notnull(), a] = np.squeeze(encoded)
        dii[a] = dii[a].astype(np.float64)

    imputer = IterativeImputer(
        estimator=BayesianRidge(),
        sample_posterior=True,
        max_iter=10,
        initial_strategy="most_frequent",
        random_state=0,
    )
    dii = pd.DataFrame(
        imputer.fit_transform(dii[attrs_incl]), columns=dii[attrs_incl].columns
    )

    # Avoid single float value attribute explosion
    for a in set(attrs_incl) & set(attrs_missing):
        ndigits = 2 if a == 'gpa' else 0
        dii[a] = dii[a].apply(lambda x: round(x, ndigits))

    dii, attrs_incl_ii = one_hot_encode(dii, attrs_enc, attrs_incl)

    # Include remaining
    for a in attrs_sens:
        dii[a] = de[a]

    dii[label] = de[label]

acc_ii, d_all_ii = score_and_print(
    df=dii,
    label=label,
    imputation=imputation,
    attrs_incl=attrs_incl_ii,
    attrs_sens=attrs_sens,
    sv_map=sv_map,
)
# All Data:1 ends here

# Only Missing Data

# Exclude complete cases for iterative imputation.


# [[file:classification.org::*Only Missing Data][Only Missing Data:1]]
diim = dii.loc[dii.index.difference(dc.index), :]
attrs_incl_iim = attrs_incl_ii.copy()

# Print table
acc_iim, d_all_iim = score_and_print(
    df=diim,
    label=label,
    imputation=imputation,
    attrs_incl=attrs_incl_iim,
    attrs_sens=attrs_sens,
    sv_map=sv_map,
)
# Only Missing Data:1 ends here

# Data Set Discrimination


# [[file:classification.org::*Data Set Discrimination][Data Set Discrimination:1]]
print("|-|")
print_tab(
    "rows",
    "rows with",
    "cols with",
    "protected",
    "privileged",
    "unprivileged",
    "c⁺",
    "majority for",
    "SPD",
    "SPD",
    "SPD",
)
print_tab(
    "",
    "missing",
    "missing",
    "attribute",
    "values",
    "values",
    "",
    "protected attr.",
    "(all rows)",
    "(rows with missing)",
    "(rows w/o missing)",
)
print_tab(*[""] * 4, *["<r>"] * 7)
print("|-|")

data_rows = []
for s, sv in sv_map.items():
    sv_counts = df[[s, label]].groupby(s).count()[label]
    data_rows.append([
        "",
        "",
        "",
        f"{s}",
        f"{sv[0]}",
        f"{sv[1]}",
        f"{label}",
        f"{sv_counts.idxmax()}"
        + f" ({sv_counts.max() / sv_counts.sum() * 100:.0f}%)",
        f"{d_all(df, s, sv, label=label):.4f}",
        f"{d_all(dism, s, sv, label=label):.4f}", # or diim
        f"{d_all(dc, s, sv, label=label):.4f}",
    ])

initial_row = [
    f"{len(df)}",
    f"{len(dism)} ({len(dism) / len(df) * 100:.0f}%)",
    f"{len(df.columns[df.isna().any()])}"
    + f" ({len(set(attrs_incl) & set(df.columns[df.isna().any()]))} used)"
]

for i, col in enumerate(initial_row):
    data_rows[0][i] = col

for row in data_rows:
    print_tab(*row)

print("|-|")
# Data Set Discrimination:1 ends here

# Model Discrimination (simple)


# [[file:classification.org::*Model Discrimination (simple)][Model Discrimination (simple):1]]
print("|-|")
print_tab(
    "protected",
    "SPD (data)",
    "Acc (all rows)",
    "SPD (all rows)",
    "Acc (with missing)",
    "SPD (with missing)",
    "Acc (w/o missing)",
    "SPD (w/o missing)",
)
print_tab("", *["<r>"] * 7)
print("|-|")

data_rows = []
for s in attrs_sens:
    data_rows.append([
        f"{s}",
        f"{d_all(df, s, sv_map[s], label=label):.4f}",
    ])
    acc_d_maps = [
        {"a": [a['acc'] for a in acc_is], "d": [d[s] for d in d_all_is]},
        {"a": [a['acc'] for a in acc_ism], "d": [d[s] for d in d_all_ism]},
        {"a": [a['acc'] for a in acc_cc], "d": [d[s] for d in d_all_cc]},
    ]
    for m in acc_d_maps:
        data_rows[-1].extend([
            f"{np.mean(m['a']):.4f} ± {np.var(m['a']):.4f}",
            f"{np.mean(m['d']):.4f} ± {np.var(m['d']):.4f}",
        ])

for row in data_rows:
    print_tab(*row)

print("|-|")
# Model Discrimination (simple):1 ends here

# Model Discrimination (iterative)


# [[file:classification.org::*Model Discrimination (iterative)][Model Discrimination (iterative):1]]
print("|-|")
print_tab(
    "protected",
    "SPD (data)",
    "Acc (all rows)",
    "SPD (all rows)",
    "Acc (with missing)",
    "SPD (with missing)",
    "Acc (w/o missing)",
    "SPD (w/o missing)",
)
print_tab("", *["<r>"] * 7)
print("|-|")

data_rows = []
for s in attrs_sens:
    data_rows.append([
        f"{s}",
        f"{d_all(df, s, sv_map[s], label=label):.4f}",
    ])
    acc_d_maps = [
        {"a": [a['acc'] for a in acc_ii], "d": [d[s] for d in d_all_ii]},
        {"a": [a['acc'] for a in acc_iim], "d": [d[s] for d in d_all_iim]},
        {"a": [a['acc'] for a in acc_cc], "d": [d[s] for d in d_all_cc]},
    ]
    for m in acc_d_maps:
        data_rows[-1].extend([
            f"{np.mean(m['a']):.4f} ± {np.var(m['a']):.4f}",
            f"{np.mean(m['d']):.4f} ± {np.var(m['d']):.4f}",
        ])

for row in data_rows:
    print_tab(*row)

print("|-|")
# Model Discrimination (iterative):1 ends here
