def d_all(d, s, sv, label="accepted", target=1):
    if len(sv) != 2:
        raise Exception(
            f"{s} requires a binary set of unique values, but was {sv}"
        )

    # P(+|s0) - P(+|s1)
    p0 = len(d[(d[label] == target) & (d[s] == sv[0])]) / len(d[d[s] == sv[0]])
    p1 = len(d[(d[label] == target) & (d[s] == sv[1])]) / len(d[d[s] == sv[1]])
    return p0 - p1


def d_expl(d, s, sv, e, label="accepted", target=1):
    if len(sv) != 2:
        raise Exception(
            f"{s} requires a binary set of unique values, but was {sv}"
        )

    de = 0
    for v in d[e].unique():
        # Σ{i} (P(e_i|s0) - P(e_i|s1)) * P(+|e_i)
        ps0 = len(d[(d[e] == v) & (d[s] == sv[0])]) / len(d[d[s] == sv[0]])
        ps1 = len(d[(d[e] == v) & (d[s] == sv[1])]) / len(d[d[s] == sv[1]])
        # pe = len(d[(d[label] == target) & (d[e] == v)]) / len(d[d[e] == v])
        pe1 = len(d[(d[e] == v) & (d[s] == sv[0])])
        if pe1 > 0:
            pe1 = (
                len(d[(d[label] == target) & (d[e] == v) & (d[s] == sv[0])])
                / pe1
            )

        pe2 = len(d[(d[e] == v) & (d[s] == sv[1])])
        if pe2 > 0:
            pe2 = (
                len(d[(d[label] == target) & (d[e] == v) & (d[s] == sv[1])])
                / pe2
            )

        de += (ps0 - ps1) * ((pe1 + pe2) / 2)

    return de
