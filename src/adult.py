from typing import List

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

from metrics import d_all, d_expl

sns.set(style="whitegrid", palette="muted", color_codes=True)
palette = sns.color_palette()


attrs: List[str] = [
    "age",
    "workclass",
    "fnlwgt",
    "education",
    "education-num",
    "marital-status",
    "occupation",
    "relationship",
    "race",
    "sex",
    "capital-gain",
    "capital-loss",
    "hours-per-week",
    "native-country",
    "income",
]

# Source data downloaded from UCI datasets:
# https://archive.ics.uci.edu/ml/datasets/Census+Income

df: pd.DataFrame = pd.read_csv(
    "../data/adult-census/adult.csv",
    index_col=False,
    header=None,
    names=attrs,
    # dtype={"complete_ts": str, "decision_ts": str, "money": pd.Int64Dtype()},
    # na_values={"complete_ts": 0, "decision": "--", "money": 0, "sent": "--"},
    skipinitialspace=True,
    low_memory=False,
)

attrs.remove("fnlwgt")
del df["fnlwgt"]


def visualize_cd(data, s, sv=["Male", "Female"]):
    global attrs

    ae: List[str] = [
        "workclass",
        "education",
        "education-num",
        "marital-status",
        "occupation",
        "relationship",
        "race",
        "age",
        "sex",
        "capital-gain",
        "capital-loss",
        "hours-per-week",
        "native-country",
    ]
    if s in ae:
        ae.remove(s)

    df = data.dropna(subset=[s])
    df["weights"] = 0
    df.loc[df.sex == "Male", "weights"] = 1 - (
        len(df[df.sex == "Male"]) / len(df)
    )
    df.loc[df.sex == "Female", "weights"] = 1 - (
        len(df[df.sex == "Female"]) / len(df)
    )
    df = df.sample(15696, weights=df.weights)
    da = d_all(df, s, sv, label="income", target=">50K")
    de = [
        (da, d_expl(df, s, sv, e, label="income", target=">50K")) for e in ae
    ]
    de = pd.DataFrame(de, columns=["d_all", "d_expl"], index=ae)
    de["d_bad"] = de.d_all - de.d_expl

    # Plot
    fig = plt.figure(figsize=(5, 5))
    with sns.axes_style("ticks"):
        axes = fig.add_subplot()
        sns.lineplot(
            data=de[["d_bad", "d_all"]],
            color=palette[0],
            markers={"d_bad": "o", "d_all": ","},
            sort=False,
        )

    # Remove spines
    sns.despine(top=True)

    # Set text labels
    axes.tick_params(labelsize=8)
    plt.xticks(rotation=45, ha="right")
    plt.xlabel("explanatory attribute")
    plt.ylabel("discrimination")
    plt.ylim(0, 0.3)
    plt.title(f"CD ({s})", fontsize=14, pad=10)
    plt.tight_layout()

    fpath = f"cond_disc_{s}.png"
    fig.savefig(fpath, dpi=150)
    plt.close()

    return fpath
