from typing import Dict, List

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from sklearn.experimental import enable_iterative_imputer  # noqa
from sklearn.ensemble import RandomForestClassifier
from sklearn.impute import KNNImputer, IterativeImputer, SimpleImputer
from sklearn.linear_model import BayesianRidge
from sklearn.metrics import (
    accuracy_score,
    confusion_matrix,
    f1_score,
    matthews_corrcoef,
    precision_score,
    recall_score,
)
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.tree import DecisionTreeClassifier

from metrics import d_all, d_expl


sns.set(style="whitegrid", palette="muted", color_codes=True)
palette = sns.color_palette()


def _org_table_explanatory_ratios(
    df: pd.DataFrame, total: str = None, ratio: str = None
):
    total = "number of applications" if total is None else total
    ratio = "acceptance rate" if ratio is None else ratio

    print("|-|")
    col_sens = [df.columns.names[0]]
    col_expl = [df.columns.names[1]]
    for label_sens in df.columns.levels[0]:
        col_sens.append(f"{label_sens}")
        for _ in range(len(df.columns.levels[1]) - 1):
            col_sens.append("")

        for label_expl in map(str, df.columns.levels[1]):
            col_expl.append(label_expl)

    print_tab(*col_sens)
    print_tab(*col_expl)

    print("|-|")
    print_tab(
        total, *map(lambda x: f"{x:.0f}", df.loc["total"]),
    )
    print_tab(
        ratio, *map(lambda x: f"{x:.2f}", df.loc["ratio"]),
    )
    print_tab(f"{df.index.name} (+)", *map(lambda x: f"{x:.0f}", df.loc[1]))
    print("|-|")


def print_tab(*args: str) -> None:
    print("|", " | ".join(args), "|")


def score_and_print(
    df: pd.DataFrame,
    label: str,
    imputation: str,
    attrs_incl: List[str],
    attrs_sens: List[str],
    sv_map: Dict[str, List],
) -> List:
    m = RandomForestClassifier(
        n_estimators=50,
        criterion="entropy",
        max_depth=50,
        min_samples_split=10,
        min_samples_leaf=2,
        max_features=0.5,
        n_jobs=-1,
        random_state=0,
        class_weight="balanced",
        # ccp_alpha=1e-15,
        # max_samples=0.75,
    )
    sss = StratifiedShuffleSplit(
        n_splits=10, test_size=1 / np.sqrt(len(attrs_incl)), random_state=0
    )
    scores = []
    splits = []
    d_alls = []
    c_mats = []
    for tr_idx, te_idx in sss.split(df[attrs_incl], df[label]):
        dtr = df.iloc[tr_idx, :]
        dte = df.iloc[te_idx, :]
        ytr = dtr[label].values
        xtr = dtr[attrs_incl].values
        yte = dte[label].values
        xte = dte[attrs_incl].values

        # Splits
        splits.append((sum(yte), len(yte)))

        # Missing value imputation
        if imputation is not None:
            if imputation == "knn":
                imputer = KNNImputer(n_neighbors=5)
            elif imputation == "iterative":
                imputer = IterativeImputer(
                    estimator=BayesianRidge(),
                    sample_posterior=True,
                    max_iter=10,
                    initial_strategy="most_frequent",
                    random_state=0,
                )
            elif imputation == "simple":
                imputer = SimpleImputer(strategy="most_frequent")
            else:
                raise ValueError(f'Imputation method "{imputation}" unknown.')

            imputer.fit(xtr)
            xtr = imputer.transform(xtr)
            xte = imputer.transform(xte)

        # Performance Measures: accuracy, etc.
        m.fit(xtr, ytr)
        ypr = m.predict(xte)
        scores.append(
            {
                "acc": accuracy_score(yte, ypr),
                "pre": precision_score(yte, ypr),
                "rec": recall_score(yte, ypr),
                "f1s": f1_score(yte, ypr),
                "mat": matthews_corrcoef(yte, ypr),
            }
        )

        # Discrimation
        dpr = dte.copy()
        dpr["predicted"] = ypr
        ds = {}
        for s, sv in sv_map.items():
            ds[s] = d_all(dpr, s, sv_map[s], label="predicted")
            ds["d_" + s] = d_all(dpr, s, sv_map[s], label="accepted")
            ds["n_" + s] = len(
                dpr[~dpr[s].isna() if s == "sex" else dpr[s] == True]
            )

        d_alls.append(ds)

        # Normalized Confusion Matrix
        cm = confusion_matrix(yte, ypr)
        c_mats.append(cm.astype("float") / cm.sum(axis=1)[:, np.newaxis])

    print("|-|")
    print_tab("", *["<r>"] * 2)
    print("|-|")
    p_mean, n_mean = np.mean([(p, t - p) for p, t in splits], axis=0)
    print_tab("avg. samples", "", f"{p_mean + n_mean:.0f}")
    print_tab("avg. split (p/n)", f"{p_mean:.0f}", f"{n_mean:.0f}")
    print("|-|")
    for s in attrs_sens:
        d_var = np.var([d["d_" + s] for d in d_alls])
        d_mean = np.mean([d["d_" + s] for d in d_alls])
        d_mean_n = np.mean([d["n_" + s] for d in d_alls])
        print_tab(
            f"spd d. ({s})", f"{d_mean_n:.0f}", f"{d_mean:.4f} ± {d_var:.4f}"
        )

    print("|-|")
    for s in attrs_sens:
        d_var = np.var([d[s] for d in d_alls])
        d_mean = np.mean([d[s] for d in d_alls])
        d_mean_n = np.mean([d["n_" + s] for d in d_alls])
        print_tab(
            f"spd m. ({s})", f"{d_mean_n:.0f}", f"{d_mean:.4f} ± {d_var:.4f}"
        )

    print("|-|")
    accs = [i["acc"] for i in scores]
    print_tab("accuracy", "", f"{np.mean(accs):.4f} ± {np.var(accs):.4f}")
    pres = [i["pre"] for i in scores]
    print_tab("precision", "", f"{np.mean(pres):.4f} ± {np.var(pres):.4f}")
    recs = [i["rec"] for i in scores]
    print_tab("recall", "", f"{np.mean(recs):.4f} ± {np.var(recs):.4f}")
    f1ss = [i["f1s"] for i in scores]
    print_tab("f1 score", "", f"{np.mean(f1ss):.4f} ± {np.var(f1ss):.4f}")
    mats = [i["mat"] for i in scores]
    print_tab(
        "matthews corr. coef", "", f"{np.mean(mats):.4f} ± {np.var(mats):.4f}"
    )
    print("|-|")
    cm = np.mean(c_mats, axis=0)
    tn, fp, fn, tp = map(lambda x: f"{x:.3f}", cm.ravel())
    print_tab("conf. matrix", "Y = 1", "Y = 0")
    print_tab("Ŷ = 1", str(tp), str(fp))
    print_tab("Ŷ = 0", str(fn), str(tn))
    print("|-| ")
    return scores, d_alls


def get_explanatory_ratios(
    df: pd.DataFrame,
    attr_sens: str = None,
    attr_expl: str = None,
    target: str = "accepted",
    target_positive=1,
    pt_val: str = "app_id",
) -> pd.DataFrame:
    df = df.copy()
    df[attr_sens].fillna("missing", inplace=True)
    df[attr_expl].fillna("missing", inplace=True)
    df = df.groupby([attr_sens, attr_expl, target]).count()
    df = df.pivot_table(
        values=pt_val,
        index=[attr_expl, attr_sens],
        columns=target,
        fill_value=0,
    )
    df["total"] = df.sum(axis=1)
    df["ratio"] = df[target_positive] / df.total
    return df.T


def one_hot_encode(
    df: pd.DataFrame, attrs_enc: List[str], attrs_incl: List[str]
) -> (pd.DataFrame, List[str]):
    attrs_incl = attrs_incl.copy()
    for a in attrs_enc:
        df = pd.concat([df, pd.get_dummies(df[a])], axis=1)
        attrs_incl.extend(filter(None.__ne__, df[a].unique()))
        if a in attrs_incl:
            attrs_incl.remove(a)

    return df, attrs_incl


def sample_uniform(data, attr, frac=None):
    df = data.copy()
    nan_val = "missing" if data[attr].dtypes == np.object else -1
    df[attr].fillna(nan_val, inplace=True)
    df["weights"] = 0
    weights = (df[attr].value_counts() / len(df[attr])) ** -3
    for a in df[attr].unique():
        df.loc[df[attr] == a, "weights"] = weights[a]

    if frac is None:
        n = df[attr].value_counts().min() * len(df[attr].unique())
        samples = df.sample(n, weights=df.weights)
    else:
        samples = df.sample(frac=frac, weights=df.weights)

    del samples["weights"]
    return samples


def visualize_discr(data, es, s, sv=[0, 1], title=None, fname=None):
    df = data.dropna(subset=[s])
    da = d_all(df, s, sv)
    de = [(da, d_expl(df, s, sv, e)) for e in es]
    de = pd.DataFrame(de, columns=["d_all", "d_expl"], index=es)
    de["d_bad"] = de.d_all - de.d_expl

    # Plot
    fig = plt.figure(figsize=(5, 5))
    with sns.axes_style("ticks"):
        axes = fig.add_subplot()
        sns.lineplot(
            data=de[["d_bad", "d_all"]],
            color=palette[0],
            markers={"d_bad": "o", "d_all": ","},
        )

    # Remove spines
    sns.despine(top=True)

    # Set text labels
    axes.tick_params(labelsize=8)
    plt.xticks(rotation=45, ha="right")
    plt.xlabel("explanatory attribute")
    plt.ylabel("discrimination")
    # plt.ylim(0)
    if title is None:
        title = f"CD ({s})"

    plt.title(title, fontsize=14, pad=10)
    plt.tight_layout()

    if fname is None:
        fname = f"cond_disc_{s}"

    fpath = f"../doc/figures/{fname}.png"
    fig.savefig(fpath, dpi=150)
    plt.close()

    return fpath
