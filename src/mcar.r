.lib <- c("BaylorEdPsych", "dplyr")
.inst <- .lib %in% installed.packages()

if (length(.lib[!.inst]) > 0)
    install.packages(
        .lib[!.inst],
        repos=c("http://rstudio.org/_packages", "http://cran.rstudio.com")
    )

lapply(.lib, require, character.only=TRUE)

df <- read.csv("../data/law-school-numbers/merged.csv")

# Select and sample
#dfc <- select(df, -c(dt_complete, dt_decision, dt_sent, user_id))
#dfc <- df[sample(nrow(df), 10000), ]

# Remove all completely null columns
#dfc <- dfc[!apply(dfc, 1, function(x) all(is.na(x))), ]

require(BaylorEdPsych)
LittleMCAR(df)
