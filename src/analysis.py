# Imports
# :PROPERTIES:
# :header-args: :results silent
# :END:
# Default data and plotting libraries


# [[file:analysis.org::*Imports][Imports:1]]
from collections import OrderedDict
from typing import List

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

sns.set_context("paper", font_scale=1.5)
sns.set(style="whitegrid", color_codes=True)
palette = sns.color_palette()
# Imports:1 ends here



# Package files


# [[file:analysis.org::*Imports][Imports:2]]
import utils
from metrics import d_all, d_expl
from utils import print_tab, visualize_discr
# Imports:2 ends here

# Applications


# [[file:analysis.org::*Applications][Applications:1]]
attrs_apps_all: List[str] = [
    "aa",
    "app_id",
    "application_id",
    "attending",
    "complete_ts",
    "decision",
    "ed",
    "feewaiver",
    "money",
    "school",
    "sent",
    "status",
    "url",
]
apps: pd.DataFrame = pd.read_csv(
    "../data/law-school-numbers/user_apps.csv",
    header=0,
    usecols=attrs_apps_all,
    dtype={"complete_ts": str, "decision_ts": str},
    na_values={"complete_ts": 0, "decision": "--", "money": 0, "sent": "--"},
    low_memory=False,
)
apps.rename(
    columns={
        "aa": "african_american",
        "app_id": "app_cycle_id",
        "application_id": "app_id",
        "complete_ts": "dt_complete",
        "decision": "dt_decision",
        "ed": "early_decision",
        "feewaiver": "fee_waiver",
        "money": "scholarship",
        "sent": "dt_sent",
        "url": "user_id",
    },
    inplace=True,
)
# Applications:1 ends here



# =apps= contains rows for every application a user makes in a =cycle= with
# =app_cycle_id= (and =user_id=) being consistent identifiers.  =lsat= reflects
# the current score for the application period determined by =cycle=.

# We identified 4 cases in =apps= where a user was identified as African American
# in one case and not another.  Amount of =scholarship= appears to be erroneously
# reported and requires defining a threshold.

# Below we filter erroneously entered data for the four date attributes.  The user
# is free to enter whatever date (past or future) they see fit.

# TODO: clean dates data based on =users= cycle
# TODO: map school identifier from separately supplied list
#       ("../data/law-school-numbers/name_conversions.csv")

# We discard =decision_ts=, as the time information is missing and thus makes it
# equivalent to =dt_decision=.


# [[file:analysis.org::*Applications][Applications:2]]
for ts_attr in ["dt_complete"]:
    ts_col = apps[ts_attr].mask(apps[ts_attr].dropna().apply(len) != 10)
    apps[ts_attr] = (
        pd.to_datetime(ts_col, unit="s")
        .dt.strftime("%m/%d/%y")
        .astype("datetime64[ns]")
    )

for ts_attr in ["dt_sent", "dt_decision"]:
    apps[ts_attr] = pd.to_datetime(apps[ts_attr], format="%m/%d/%y")
# Applications:2 ends here



# Furthermore, we extract the target variable to a new field called =accepted=.


# [[file:analysis.org::*Applications][Applications:3]]
attr_target = "accepted"
attr_positive = 1
attr_negative = 0
apps["ac"] = np.where(apps.status.isin(["Ac", "AcWa"]), attr_positive, np.nan)
apps["re"] = np.where(apps.status.isin(["Re", "ReWa"]), attr_negative, np.nan)
apps[attr_target] = (
    apps.pop("ac").fillna(apps.pop("re")).astype(pd.Int64Dtype())
)
# Applications:3 ends here

# Schools


# [[file:analysis.org::*Schools][Schools:1]]
schools = pd.read_csv(
    "../data/law-school-numbers/school_names.csv",
    names=["id", "full_name", "short_name", "rank"],
    dtype={"rank": int},
    low_memory=False,
)
# Schools:1 ends here

# Users


# [[file:analysis.org::*Users][Users:1]]
attrs_users_all: List[str] = [
    "app_id",
    "cycle",
    "gpa",
    "international",
    "lgbt",
    "lsat",
    "major",
    "military",
    "nontrad",
    "race",
    "schooltype",
    "sex",
    "state",
    "tfa",
    "url",
    "urm",
    "yearsout",
]
users: pd.DataFrame = pd.read_csv(
    "../data/law-school-numbers/user_pages.csv",
    header=0,
    usecols=attrs_users_all,
    dtype={"cycle": str},
    na_values={
        "gpa": [0, "nan", "none"],
        "lsat": "none",
        "sex": "none",
        "state": ["Other", "none", "other"],
        "yearsout": "none",
    },
    low_memory=False,
)
users.rename(
    columns={
        "app_id": "app_cycle_id",
        "lgtb": "lgtbq",
        "major": "undergrad_major",
        "military": "veteran",
        "nontrad": "non_traditional",
        "schooltype": "undergrad_school_type",
        "tfa": "teach_for_america",
        "url": "user_id",
        "urm": "minority",
        "yearsout": "years_out",
    },
    inplace=True,
)
# Users:1 ends here



# =users= contains all user profile data for a specific profile ID (=user_id=),
# but partially duplicated for multiple application periods with =app_cycle_id=
# (and =cycle=).

# We disregard =last1= through =lsat3=, as these =lsat= equals =lsat= in 98.36%
# and the other two occur in less than 2% of total applications.


# [[file:analysis.org::*Users][Users:2]]
users.sex = users.sex.dropna().apply(str.lower)
users.state = users.state.dropna().apply(str.title)
users.years_out = users.years_out.dropna().apply(str.lower)
# Users:2 ends here



# Define list of sensitive attributes.


# [[file:analysis.org::*Users][Users:3]]
attrs_sensitive = [
    "african_american",
    "lgbt",
    "sex",
    "minority",
]
# Users:3 ends here

# Merged

# Merge users and applications data sets.  *Ignore* users who don't have
# applications or vice versa, and only consider applications that have been either
# accepted or rejected.  Additionally, we drop all unnecessary columns and remove
# duplicates for users who applied to schools multiple times over multiple cycles.
# We choose to keep the most recent (="last"=) application, as later applications
# have a higher acceptance rate.


# [[file:analysis.org::*Merged][Merged:1]]
df = (
    apps[~apps.accepted.isna()]
    .merge(users, how="inner", on=["user_id", "app_cycle_id"])
    .drop(
        [
            # "attending",
            # "dt_complete",
            # "dt_decision",
            # "dt_sent",
            "race",
            # "status",
            "undergrad_major",
            "undergrad_school_type",
        ],
        axis=1,
    )
    .sort_values(["cycle", "app_id"])
    .drop_duplicates(["user_id", "school"], keep="last", ignore_index=True)
)

# Since all accepted have a value now
df.accepted = df.accepted.astype(int)
# Merged:1 ends here

# General


# [[file:analysis.org::*General][General:1]]
mean_app_counts: int = apps.groupby("user_id").count().mean().app_id
mean_acc_counts: int = (
    apps[apps.accepted == 1].groupby("user_id").count().mean().app_id
)
mean_rej_counts: int = (
    apps[apps.accepted == 0].groupby("user_id").count().mean().app_id
)

print("|-|")
print_tab("Unique users", f"{len(users['user_id'].unique())}")
print_tab("Unique users with applications", f"{len(apps.user_id.unique())}")
print_tab("Average applications per user", f"{mean_app_counts:.3f}")
print_tab("Average acc. apps per user", f"{mean_acc_counts:.3f}")
print_tab("Average rej. apps per user", f"{mean_rej_counts:.3f}")
print_tab("Unique applications", f"{len(apps)}")
print_tab("Accepted apps", f"{len(df[df.accepted == 1])}")
print_tab("Rejected apps", f"{len(df[df.accepted == 0])}")
print_tab("Accepted users", f"{len(df[df.accepted == 1].user_id.unique())}")
print_tab("Rejected users", f"{len(df[df.accepted == 0].user_id.unique())}")
print("|-|")
# General:1 ends here

# Sensitive Attributes

# We further differentiate based on sensitive attributes.

# Underrepresented minorities are self-identified and may or may not belong to
# other sensitive groups.


# [[file:analysis.org::*Sensitive Attributes][Sensitive Attributes:1]]
cols = OrderedDict(
    [
        ("app_id", "applications"),
        ("user_id", "users"),
        ("scholarship", "scholarship"),
        ("state", "states"),
    ]
)

print(f"|-|\n|Totals|{'|'.join(cols.values())}|\n|-|")
for s in attrs_sensitive:
    values = df.fillna("missing").groupby(s).nunique()
    for i in values.index:
        apps_stats = values[cols.keys()].loc[i]
        print_tab(
            f"{s} ({i})", *map(str, apps_stats),
        )

print("|-|")
# Sensitive Attributes:1 ends here

# Number of Applications


# [[file:analysis.org::*Number of Applications][Number of Applications:1]]
_df = df.copy()
keys = pd.DataFrame({"": []}).describe().index
print(f"|-|\n|Applications|{'|'.join(keys)}|\n|-|")
for s in attrs_sensitive:
    if _df[s].dtypes == np.int64:
        missing_value = -1
    elif _df[s].dtypes == np.object:
        missing_value = "missing"

    _df[s].fillna(missing_value, inplace=True)
    values = _df[s].unique()

    # Move missing value to last position
    if missing_value in values:
        values = values[values != missing_value]
        values = np.append(values, missing_value)

    # Ratios of accepted applications
    for v in values:
        apps_stats = (
            _df[_df[s] == v][["app_id", "user_id"]]
            .groupby("user_id")
            .count()
            .app_id.describe()
        )
        print_tab(
            f"{s} ({v})", *map(lambda x: f"{x:.3f}", apps_stats),
        )

print("|-|")
del _df
# Number of Applications:1 ends here

# Overall Acceptance Ratios


# [[file:analysis.org::*Overall Acceptance Ratios][Overall Acceptance Ratios:1]]
_df = df.copy()
print(f"|-|\n|Sensitive Attribute|Rate|\n|-|")
for s in attrs_sensitive:
    if _df[s].dtypes == np.int64:
        missing_value = -1
    elif _df[s].dtypes == np.object:
        missing_value = "missing"

    _df[s].fillna(missing_value, inplace=True)
    values = _df[s].unique()

    # Move missing value to last position
    if missing_value in values:
        values = values[values != missing_value]
        values = np.append(values, missing_value)

    # Ratios of accepted applications
    for v in values:
        apps_accepted = len(_df[(_df[s] == v) & (_df.accepted == 1)])
        apps_total = len(_df[_df[s] == v])
        print_tab(f"{s} ({v})", f"{apps_accepted / apps_total:.3f}")

print("|-|")
del _df
# Overall Acceptance Ratios:1 ends here

# Acceptance Ratios per User


# [[file:analysis.org::*Acceptance Ratios per User][Acceptance Ratios per User:1]]
keys = pd.DataFrame({"": []}).describe().index
print(f"|-|\n|User Acceptance Ratios|{'|'.join(keys)}|\n|-|")
for s in attrs_sensitive:
    _df = df.copy()
    if _df[s].dtypes == np.int64:
        missing_value = -1
    elif _df[s].dtypes == np.object:
        missing_value = "missing"

    _df[s].fillna(missing_value, inplace=True)
    values = _df[s].unique()

    # Move missing value to last position
    if missing_value in values:
        values = values[values != missing_value]
        values = np.append(values, missing_value)

    # Ratios of accepted applications
    for v in values:
        accept_sum = (
            _df[_df[s] == v][["accepted", "user_id"]].groupby("user_id").sum()
        )
        apps_count = (
            _df[_df[s] == v][["app_id", "user_id"]].groupby("user_id").count()
        )
        apps_stats = (accept_sum.accepted / apps_count.app_id).describe()
        print_tab(
            f"{s} ({v})", *map(lambda x: f"{x:.3f}", apps_stats),
        )

print("|-|")
# Acceptance Ratios per User:1 ends here

# African American / LGBT / Underrepresented Minorities

# These attribute may only be useful when compared to all applications. The value
# =0= merely implies that the information is not provided, that is neither
# missing, nor supplied.  There may be a large number of applicants for whom this
# attribute has not been inferred properly or is missing completely.


# [[file:analysis.org::*African American / LGBT / Underrepresented Minorities][African American / LGBT / Underrepresented Minorities:1]]
utils._org_table_explanatory_ratios(
    utils.get_explanatory_ratios(
        df, attr_sens="lgbt", attr_expl="international"
    )
)
# African American / LGBT / Underrepresented Minorities:1 ends here

# Sex -> Teach For America


# [[file:analysis.org::*Sex -> Teach For America][Sex -> Teach For America:1]]
utils._org_table_explanatory_ratios(
    utils.get_explanatory_ratios(
        df, attr_sens="sex", attr_expl="teach_for_america"
    )
)
# Sex -> Teach For America:1 ends here

# Conditional Discrimination

# Define all explanatory attributes.


# [[file:analysis.org::*Conditional Discrimination][Conditional Discrimination:1]]
attrs_expl = [
    "african_american",
    "cycle",
    "early_decision",
    "fee_waiver",
    "gpa",
    "international",
    "lgbt",
    "lsat",
    "minority",
    "non_traditional",
    "school",
    "sex",
    "state",
    "teach_for_america",
    "veteran",
    "years_out",
]
# Conditional Discrimination:1 ends here



# Why is conditional treatment necessary?
# - equal treatment of groups may disregard objectively explainable differences
# - by removing explainable discrimination, we can correctly mitigate real bias

# Why is there no further research, or at least references to conditional
# treatment?  My assumption is, that this research is now part of the
# explainability branch of FATML, and that the requirements for the basis of
# conditional treatment are too limiting for most cases (?).

# We define a privileged attribute mapping.


# [[file:analysis.org::*Conditional Discrimination][Conditional Discrimination:2]]
sv_map = {
    "african_american": [0, 1],
    "lgbt": [0, 1],
    "minority": [0, 1],
    "sex": ["male", "female"],
}
# Conditional Discrimination:2 ends here



# Visualize conditional discrimination for each sensitive attribute.


# [[file:analysis.org::*Conditional Discrimination][Conditional Discrimination:3]]
results = {}
es = set(attrs_expl) - set(sv_map.keys())
for s, sv in sv_map.items():
    _df = df.copy()
    _df.dropna(subset=[s], inplace=True)
    # es = set(attrs_expl) - {s}
    dall = d_all(_df, s, sv)
    dexp = [(dall, d_expl(_df, s, sv, e)) for e in es]
    dexp = pd.DataFrame(dexp, columns=["d_all", "d_expl"], index=es)
    dexp["d_bad"] = dexp.d_all - dexp.d_expl
    results[s] = dexp

# _results = results.copy()
del _df
# Conditional Discrimination:3 ends here



# #+RESULTS[8817f7a541272957a39c475c73dc7b8cf3bdb855]:


# [[file:analysis.org::*Conditional Discrimination][Conditional Discrimination:4]]
# plt.rcParams["text.latex.preamble"] = ["\usepackage{amsfonts}"]
# plt.rcParams["text.usetex"] = True

# da_tex = "$\mathfrak{D}_{\text{all}}$"
# db_tex = "$\mathfrak{D}_{\text{bad}}$"

# results = _results.copy()
# for s in sv_map.keys():
#     results[s] = results[s].rename(columns={"d_all": da_tex, "d_bad": db_tex})
#     results[s].index = results[s].index.str.replace("_", "\_")
#     results[s.replace('_', '\\_')] = results.pop(s)

# Plot
fig = plt.figure(figsize=(9, 9))
with sns.axes_style("ticks"):
    for i, s in enumerate(results.keys(), 1):
        axes = fig.add_subplot(2, 2, i)
        sns.lineplot(
            data=results[s][["d_bad", "d_all"]],
            # data=results[s][[da_tex, db_tex]],
            palette=sns.color_palette("Set1", 2),
            markers={"d_all": ",", "d_bad": "o"},
            legend="full" if (i == 1) else False,
            ax=axes,
        )
        # Remove spines
        sns.despine(top=True)
        plt.title(s, fontsize=12, pad=10)
        plt.xlim(-0.1, len(es) - 0.9)
        # Set text labels
        axes.tick_params(length=4, labelsize=12)
        axes.xaxis.set_ticklabels([]) if (i < 3) else None
        if (i > 2):
            plt.xticks(rotation=45, ha="right")

fig.text(0.5, 0.025, "Explanatory Attribute", ha="center", weight="bold")
fig.text(0.025, 0.5, "Discrimination", va="center", rotation="vertical", weight="bold")
#plt.tight_layout()
plt.gcf().subplots_adjust(left=0.12, bottom=0.18)

fname = "conditional_discrimination"
fpath = f"../doc/figures/{fname}.png"
fig.savefig(fpath, dpi=300)
plt.close()

plt.rcParams['text.usetex'] = False
fpath
# Conditional Discrimination:4 ends here



# #+RESULTS[60b522567c25b7fb443ffac17d535b7f81169408]:
# [[file:../doc/figures/conditional_discrimination.png]]

# file:../doc/figures/cond_disc_african_american.png
# file:../doc/figures/cond_disc_lgbt.png
# file:../doc/figures/cond_disc_minority.png
# file:../doc/figures/cond_disc_sex.png

# We further sample the data and compare the results.


# [[file:analysis.org::*Conditional Discrimination][Conditional Discrimination:5]]
for s, sv in sv_map.items():
    data = utils.sample_uniform(df.dropna(subset=[s]), s, frac=0.1)
    es = set(attrs_expl) - {s}
    print(
        "file:"
        + visualize_discr(
            data, es, s, sv_map[s], fname=f"cond_disc_{s}_sampled"
        )
    )
# Conditional Discrimination:5 ends here



# #+RESULTS[0eb330cae2da15beaa7c464f90162eb242d4544b]:
# file:../doc/figures/cond_disc_african_american_sampled.png
# file:../doc/figures/cond_disc_lgbt_sampled.png
# file:../doc/figures/cond_disc_minority_sampled.png
# file:../doc/figures/cond_disc_sex_sampled.png

# Binning =lsat= and =gpa= together.


# [[file:analysis.org::*Conditional Discrimination][Conditional Discrimination:6]]
data = df.dropna(subset=["gpa", "lsat"]).copy()
data["gpa"] = data.gpa.apply(lambda x: round(x, 1))
data["gpa+lsat"] = list(zip(data.gpa, data.lsat))
print(
    "file:"
    + visualize_discr(
        data,
        set(attrs_expl).union({"gpa+lsat"}) - {"sex"},
        "sex",
        sv_map["sex"],
        title=f"sex, bin=gpa+lsat, n={len(data)}",
        fname=f"cond_disc_sex_gpa+lsat",
    )
)
# Conditional Discrimination:6 ends here



# #+RESULTS[d0cb9be6816c12f9f3a7c58b34afac7e564abf7d]:
# file:../doc/figures/cond_disc_sex_gpa+lsat.png

# Observe distribution of =gpa= vs. =lsat= for accepted and rejected applications.


# [[file:analysis.org::*Conditional Discrimination][Conditional Discrimination:7]]
data = df.dropna(subset=["gpa", "lsat"])
data = data[data.school == "gulc"]  # .sample(frac=0.05)

# Plot accepted
with sns.axes_style("ticks"):
    g = sns.jointplot(
        x="gpa", y="lsat", data=data[data.accepted == 1], kind="hex"
    )

# Set text labels
g.ax_joint.axes.tick_params(labelsize=8)
g.fig.suptitle(f"accepted, n={len(data[data.accepted == 1])}", y=1.02)

fpath = f"../doc/figures/jointplot_gpa_lsat_accepted.png"
g.savefig(fpath, dpi=150)
print("file:" + fpath)
plt.close(g.fig)

# Plot rejected
with sns.axes_style("ticks"):
    g = sns.jointplot(
        x="gpa", y="lsat", data=data[data.accepted == 0], kind="hex"
    )

# Set text labels
g.ax_joint.axes.tick_params(labelsize=8)
g.fig.suptitle(f"rejected, n={len(data[data.accepted == 0])}", y=1.02)

fpath = f"../doc/figures/jointplot_gpa_lsat_rejected.png"
g.savefig(fpath, dpi=150)
print("file:" + fpath)
plt.close(g.fig)
# Conditional Discrimination:7 ends here



# #+RESULTS[8da394ea12fbe4c7936a7604b123f1b6b9ac1238]:
# : file:../doc/figures/jointplot_gpa_lsat_accepted.png
# : file:../doc/figures/jointplot_gpa_lsat_rejected.png

# Analyse =years_out= explainability further for =sex= attribute.


# [[file:analysis.org::*Conditional Discrimination][Conditional Discrimination:8]]
for yo in df.years_out.unique():
    cond = df.years_out == yo
    if pd.isna(yo):
        cond = pd.isna(df.years_out)

    data = df[cond].dropna(subset=["sex"])
    data = utils.sample_uniform(data, "sex")
    es = set(attrs_expl) - {"sex"}
    print(
        "file:"
        + visualize_discr(
            data,
            es,
            "sex",
            sv_map["sex"],
            title=f"sex, years_out = {yo}, n={len(data)}",
            fname=f"cond_disc_sex_yo_{'_'.join(str(yo).split())}",
        )
    )
# Conditional Discrimination:8 ends here

# Missingness


# [[file:analysis.org::*Missingness][Missingness:1]]
na_ratios = df.isna().sum() / len(df)
na_ratios.sort_values(ascending=False, inplace=True)
na_ratios = pd.concat(
    [
        na_ratios[na_ratios > 0],
        na_ratios[na_ratios == 0].sort_index(ascending=True),
    ]
)
# Missingness:1 ends here

# Print ratios of missing values of the users data set


# [[file:analysis.org::*Print ratios of missing values of the users data set][Print ratios of missing values of the users data set:1]]
# Plot data
fig, axes = plt.subplots(figsize=(5, 5))
users_na_ratios: pd.Series = users.isna().sum() / len(users)
users_na_ratios.sort_index(ascending=False, inplace=True)
users_na_ratios.sort_values(ascending=False, inplace=True)
sns.barplot(x=users_na_ratios, y=users_na_ratios.index, color=palette[0])

# Remove spines
sns.despine(top=True, bottom=True)
axes.xaxis.tick_top()
axes.tick_params(length=0)

# Set text labels
plt.title("Proportion of Missingness (Users)", fontsize=14, pad=10)
plt.tight_layout()

fname = "users_na_ratios.png"
fpath = f"../doc/figures/{fname}"
fig.savefig(fpath, dpi=150)
plt.close()
fpath
# Print ratios of missing values of the users data set:1 ends here

# Print ratios of missing values of the applications data set


# [[file:analysis.org::*Print ratios of missing values of the applications data set][Print ratios of missing values of the applications data set:1]]
# Plot data
fig, axes = plt.subplots(figsize=(5, 5))
apps_na_ratios: pd.Series = apps.isna().sum() / len(apps) * 100
apps_na_ratios.sort_index(ascending=False, inplace=True)
apps_na_ratios.sort_values(ascending=False, inplace=True)
sns.barplot(x=apps_na_ratios, y=apps_na_ratios.index, color=palette[0])

# Remove spines
sns.despine(top=True, bottom=True)
axes.xaxis.tick_top()
axes.tick_params(length=0)

# Set text labels
plt.title("Proportion of Missingness (Applications)", fontsize=14, pad=10)
plt.tight_layout()

# Save plot
fname = "apps_na_ratios.png"
fpath = f"../doc/figures/{fname}"
fig.savefig(fpath, dpi=150)
plt.close()
fpath
# Print ratios of missing values of the applications data set:1 ends here

# Print ratios of missing values of merged users / applications data set


# [[file:analysis.org::*Print ratios of missing values of merged users / applications data set][Print ratios of missing values of merged users / applications data set:1]]
grid_kws = {"height_ratios": (1, 24), "hspace": 0.03}
fig, axes = plt.subplots(2, figsize=(7, 9), gridspec_kw=grid_kws)
with sns.axes_style("whitegrid"):
    sns.barplot(
        x=na_ratios,
        y=na_ratios.index,
        linewidth=0.75,
        edgecolor="black",
        color="lightgray",
        ax=axes[1],
    )

# Remove spines
sns.despine(top=False, bottom=True, left=True, offset=5)
axes[0].axis("off")
axes[1].spines["top"].set_color("black")
axes[1].grid(False)
axes[1].xaxis.tick_top()
axes[1].set_xlim(xmax=0.7)
axes[1].tick_params(labelsize=12)

# Set text labels
fig.suptitle("Proportion of Missingness", fontsize=20, y=0.95)
plt.gcf().subplots_adjust(left=0.22)
plt.tight_layout()

# Save plot
fname = "merged_na_ratios_all.png"
fpath = f"../doc/figures/{fname}"
fig.savefig(fpath, dpi=150)
plt.close()
fpath
# Print ratios of missing values of merged users / applications data set:1 ends here

# Missingness cross analysis


# [[file:analysis.org::*Missingness cross analysis][Missingness cross analysis:1]]
attrs_date = ["dt_sent", "dt_decision", "dt_complete"]
attrs_miss = list(set(df.columns[df.isnull().any()]) - set(attrs_date))
attrs_miss = [a for a in na_ratios.index if a in attrs_miss]

patterns = (
    df.isna()
    .groupby(list(na_ratios.index))
    .size()
    .reset_index()
    .rename(columns={0: "records"})
)
patterns.sort_values(by="records", ascending=False, inplace=True)
patterns = patterns[patterns.records > (len(df) * 0.01)].reset_index(drop=True)

# Plot data
grid_kws = {"height_ratios": (1, 20), "hspace": 0.03}
fig, axes = plt.subplots(2, figsize=(7, 9), gridspec_kw=grid_kws)
with sns.axes_style("white"):
    sns.barplot(
        x=patterns.records,
        y=patterns.index,
        linewidth=0.75,
        edgecolor="black",
        color="lightgray",
        ax=axes[0],
    )
    sns.heatmap(
        patterns[list(na_ratios.index)].transpose(),
        linecolor="k",
        linewidth=0.5,
        cbar=False,
        cmap=["white", "lightgray"],
        xticklabels=False,
        yticklabels=True,
        ax=axes[1],
    )

# Remove spines
sns.despine(top=True, bottom=True, right=True, left=True)
axes[0].grid(False)
axes[0].set_xlabel("")
axes[0].set_xticks([])
axes[0].set_yticks([])
axes[1].set_xlabel("")
axes[1].tick_params(labelsize=12)

# Set text labels
fig.suptitle("Missingness Pattern", fontsize=20, y=0.95)
plt.gcf().subplots_adjust(left=0.22)
plt.tight_layout()

# Save plot
fname = "merged_na_cross_all.png"
fpath = f"../doc/figures/{fname}"
fig.savefig(fpath, dpi=150)
plt.close()
fpath
# Missingness cross analysis:1 ends here

# Visualize missing value occurrence over time


# [[file:analysis.org::*Visualize missing value occurrence over time][Visualize missing value occurrence over time:1]]
# Plot data
fig, axes = plt.subplots(figsize=(10, 5))
sns.heatmap(
    df.isna().sample(2500).transpose(),
    cbar=False,
    xticklabels=False,
    yticklabels=True,
    annot_kws={"fontsize": 8},
)

# Set text labels
axes.tick_params(labelsize=8)
plt.title("Missing Value Occurrence (Merged)", fontsize=14, pad=10)
plt.tight_layout()

# Save plot
fname = "merged_na_occurrence.png"
fpath = f"../doc/figures/{fname}"
fig.savefig(fpath, dpi=300)
plt.close()
fpath
# Visualize missing value occurrence over time:1 ends here

# Missing to missing


# [[file:analysis.org::*Missing to missing][Missing to missing:1]]
merged_na_corrs = df[na_ratios.index].iloc[
    :,
    [
        i
        for i, n in enumerate(
            np.var(df[na_ratios.index].isnull(), axis="rows")
        )
        if n > 0
    ],
]
merged_na_corrs = merged_na_corrs.isna().astype(int)
merged_na_corrs = pd.concat(
    [merged_na_corrs.drop("sex", axis=1), df[attrs_sensitive + ["accepted"]]],
    axis=1
)
merged_na_corrs.sex = (merged_na_corrs.sex != "male").astype(int)
for i in attrs_sensitive:
    merged_na_corrs[i] = 1 - merged_na_corrs[i]

merged_na_corrs = merged_na_corrs.corr()
mask = np.zeros_like(merged_na_corrs)
mask[np.triu_indices_from(mask)] = True
# np.fill_diagonal(mask, 0)

# Plot
fig, axes = plt.subplots(figsize=(9, 7))
sns.heatmap(
    merged_na_corrs,
    mask=mask,
    cbar=True,
    vmin=-1,
    vmax=1,
    cmap="RdBu",
    annot=True,
    annot_kws={"size": 7.5},
    fmt=".2f",
    xticklabels=True,
    yticklabels=True,
    square=True
)

# Set text labels
axes.tick_params(labelsize=10)
#fig.suptitle("Missing Value Correlations", fontsize=15, y=0.95)
plt.xticks(rotation=45, ha="right")
plt.title("Missing Value Correlations", fontsize=15, pad=20)
plt.tight_layout()

# Save plot
fname = "merged_na_corrs.png"
fpath = f"../doc/figures/{fname}"
fig.savefig(fpath, dpi=150)
plt.close()
fpath
# Missing to missing:1 ends here

# Sensitive values to missing


# [[file:analysis.org::*Sensitive values to missing][Sensitive values to missing:1]]
na_indices = [
    i for i, n in enumerate(np.var(df.isnull(), axis="rows")) if n > 0
]
for s in attrs_sensitive:
    unique_values = df[s].unique()
    n = len(unique_values) * 100 + 10

    # Plot
    fig, axes = plt.subplots(figsize=(10, 10))
    for j, v in enumerate(unique_values, start=1):
        cond = df[s].isnull() if pd.isnull(v) else df[s] == v
        na_corrs = df[cond].iloc[:, na_indices].isna().corr()
        mask = np.zeros_like(na_corrs)
        mask[np.triu_indices_from(mask)] = True
        plt.subplot(n + j)
        sns.heatmap(
            na_corrs,
            mask=mask,
            cbar=False,
            vmin=-1,
            vmax=1,
            cmap="RdBu",
            annot=True,
            annot_kws={"size": 8},
            xticklabels=True,
            yticklabels=True,
        )

        # Set text labels
        axes.tick_params(labelsize=8)
        plt.xticks(rotation=45, ha="right")
        plt.title(" ".join(s.split("_")) + f" ({v})", fontsize=14, pad=10)

    # Save plot
    plt.tight_layout()
    fname = f"na_corrs_{s}.png"
    fpath = f"../doc/figures/{fname}"
    print("file:" + fpath)
    fig.savefig(fpath, dpi=130)
    plt.close()
# Sensitive values to missing:1 ends here

# Exporting


# [[file:analysis.org::*Exporting][Exporting:1]]
df.to_csv("../data/law-school-numbers/merged.csv")
df.to_parquet("../data/law-school-numbers/merged.parquet")
# Exporting:1 ends here
